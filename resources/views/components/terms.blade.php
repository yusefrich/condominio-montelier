<div class="sidemenu_360">
    <ul class="pl-0 text-light">
        <li>
            <a style="width: 100%" href="{{ route('index') }}">
                {{-- <img src="{{asset('assets_front/img/logo-dark.svg')}}" class="py-32" width="auto" height="auto" alt=""
                    loading="lazy"> --}}
                    <img src="{{asset('assets_front/img/cond-logo-full.png')}}" class="py-32 d-none d-md-block" width="auto" height="180px" alt="" loading="lazy">
                    <img src="{{asset('assets_front/img/cond-logo-full.png')}}" class="py-32 d-md-none"width="60%" height="auto" alt="" loading="lazy">

            </a>
        </li>
    </ul>
</div>

<div class="container mx-auto">
    <h1 class="text-dark text-uppercase mb-60 mt-150 pt-100 d-none d-md-block">Política de Privacidade</h1>
    <h1 style="font-size: 24px" class="text-dark text-uppercase mb-60 mt-150 d-md-none">Política de Privacidade</h1>
    {!! $support->pc_txt !!}
</div>
