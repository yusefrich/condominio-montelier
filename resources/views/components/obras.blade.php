<div id="obras" class="bg-dark ">
    <div class="container-large mx-auto ">

    
    <h1 data-aos="fade-right" class="text-light text-uppercase pt-100 px-3 pt-mdb-80 mb-32">acompanhe as obras</h1>
<div data-aos="fade-right"  class="d-block d-md-flex  px-3">
    <div class="btn-group mr-32 mb-40 btn-filter-group" role="group" aria-label="Basic example">
        @foreach($cons['years'] as $key => $year)
        <button
            data-bind="
                click: getImages.bind(this, '{{$key}}', ''),
                css: { 'active' :  year() == '{{$key}}'}
                "
            class="btn btn-outline-light">{{$key}}</button>
        @endforeach
    </div>
    
    <div class="btn-group mb-40 btn-filter-group" role="group" aria-label="Basic example">
        @foreach($cons['months'] as $key => $month)
        <button
            data-bind="
                click: getImages.bind(this, '', '{{$key}}'),
                css: { 'active' :  month() == '{{$key}}'}
                "
            class="btn btn-outline-light">{{$month}}</button>
        @endforeach
    </div>
</div>

</div>
@include('components._lib')
</div>


@push('scripts')
<script>
    function vm() {
        var self = this;
        self.year = ko.observable('');
        self.month = ko.observable('');
        self.images = ko.observableArray([]);

        self.getImages = function(year = '', month = '') {
            if (year) {
                self.year(year);
            }
            if (month) {
                self.month(month);
            }

            var year = year ? year : self.year;
            var month = month ? month : self.month;
            $.get("{{route('api.photos')}}", {year: year, month: month })
            .done(function(data) {
                self.images($.map(data, function (item) {
                    return new image(item);
                }));
            }); 
        }
    }

    function image(data) {
        var self = this;
        self.url = ko.observable(data.url);
        self.id = ko.observable(data.id);
        self.title = ko.observable(data.title);
    }

    function setCarouselModalObras(element) {
        var str = element.id.split("-")
        $('#carousel_img_obras').carousel(+str[str.length - 1]);
    }
    var vm = new vm();
    ko.applyBindings(vm);
    vm.getImages("{{$cons['default']['year']}}", "{{$cons['default']['month']}}");
</script>
@endpush

