<div id="buy" class="container-large mx-auto pt-100 pt-mdb-80 mb-100 mb-mdb-80">
    <div class="row mx-0">
        <div data-aos="fade-right"  class="col-md-3">
            <h1 class="text-dark text-uppercase mb-32">COMPRE ONLINE</h1>
            <p class="text-dark mb-32">{{$support->co_txt}}</p>
            <a target="_blank" href="{{$support->co_bt_url}}" class="btn btn-outline-dark btn-spacing mb-32"><span>{{$support->co_bt_txt}}</span> <i class="icon {{$support->co_bt_ico}} ml-16"></i></a>
        </div>
        <div class="col-md-9">
            <div class="pratic-holder">
                <h2 data-aos="fade-right" class="text-light text-uppercase mb-4">Veja como é Prático</h2>
                <div class="row pratic-steps">
                    <div data-aos="fade-down" data-aos-delay="300" class="col-md-4 pratic-step">
                        <div class="d-flex mb-3 mt-3 mt-md-0">
                            <h1>1</h1>
                            <img class="img-fluid" src="{{url('storage/support/'.$support->co_1_ico)}}" alt="">
                        </div>
                        <h3 class="text-light text-uppercase ">{{$support->co_1_txt}}</h3>        
                    </div>
                    <div data-aos="fade-down" data-aos-delay="600" class="col-md-4 pratic-step">
                        <div class="d-flex mb-3 mt-3 mt-md-0">
                            <h1>2</h1>
                            <img class="img-fluid position-relative" src="{{url('storage/support/'.$support->co_2_ico)}}" height="100%" alt="">
                        </div>
                        {{-- <span class="align-bottom">bottom</span> --}}

                        <h3 class="text-light text-uppercase pt-2">{{$support->co_2_txt}}</h3>        
                    </div>
                    <div data-aos="fade-down" data-aos-delay="900" class="col-md-4 pratic-step">
                        <div class="d-flex mb-3 mt-3 mt-md-0">
                            <h1>3</h1>
                            <img class="img-fluid position-relative" src="{{url('storage/support/'.$support->co_3_ico)}}" alt="">
                        </div>
                        <h3 class="text-light text-uppercase pt-2">{{$support->co_3_txt}}</h3>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
