@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Contatos</h1>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>
              <i class="far fa-user lga"></i>
              Listagem<br>
              <small>{{ $data->total() }} resultados.</small>
            </h4>
          </div>
          <div class="card-body -table">
            <div class="table-responsive">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    <th>Book</th>
                    <th>Tabela</th>
                  </tr>
                  @foreach($data as $d)
                    <tr>
                      <td>{{ $d->name }}</td>
                      <td>{{ $d->phone }}</td>
                      <td>{{ $d->email }}</td>
                      <td>@if($d->book)<i class="fa fa-check"></i>@endif</td>
                      <td>@if($d->table)<i class="fa fa-check"></i>@endif</td>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer">
            <div class="float-right">
              <nav>
                {!! $data->render() !!}
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
