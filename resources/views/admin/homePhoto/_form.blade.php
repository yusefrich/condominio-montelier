<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Img', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/home/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img') !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('title', 'Legenda', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
  </div>
</div>
