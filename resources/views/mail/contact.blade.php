@component('mail::message')
<h4>Olá, {{$data->name}}</h4><br><br>

<p>Obrigado pelo seu interesse no Condomínio Montélier. Caso queira mais alguma informação entre em contato com nossa equipe http://novo.condominiomontelier.com.br/#contact</p>
<p>Acesse nosso book: http://novo.condominiomontelier.com.br/BookMontelier.pdf</p>

@endcomponent
