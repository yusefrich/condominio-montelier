@component('mail::message')
<h4>Novo contato em {{config('app.name')}}</h4><br><br>

Nome: <b>{{$data->name}}</b><br>
Email: <b>{{$data->email}}</b><br>
Telefone: <b>{{$data->phone}}</b><br>

@endcomponent
