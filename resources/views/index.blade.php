@extends('layout.default')

@section('navigation')
@include('components._home_navigation')
@endsection

@section('content')
<div class="smoothscroll" >
    <div class="allContent">
        <div class="allContent__body">


            <div class="container-fluid p-0 ">
                @include('components.360')
                @include('components.about')
                @include('components.about_list')
                @include('components.regiao')
                @include('components.regiao_about')
                @include('components.regiao_list')
                @include('components.loteamento')
                @include('components.loteamento_map')
                @include('components.projeto')

                @include('components.downloads_cond')
                @include('components.chales')
                @include('components.promo')
                {{-- @include('components.map')
                @include('components.buy') --}}
                @include('components.contact_cond')
                {{-- @include('components.contact') --}}
            </div>
        </div>
    </div>
    <div class="allContent--hitbox"></div>
</div>

@endsection

@push('scripts')
<script>
    function setCarouselModal(carouselId, number) {
        $('#'+carouselId).carousel(number);
    }

    function scrollLiv(id, dir) {
        console.log('being called with');
        console.log(id);

        if(dir == "right"){
            document.getElementById(id).scrollLeft += 350;
        }else if(dir == "left") {
            document.getElementById(id).scrollLeft -= 350;
        }

        //$('#'+id).scrollLeft = value;
    }

</script>
<script>

</script>

@endpush