<?php

namespace App\Http\Controllers;

use App\Mail\AdminContact;
use App\Mail\Contact as MailContact;
use App\Models\Contact;
use App\Models\Support;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('store');
    }

    public function index()
    {
        $data = Contact::orderBy('id', 'desc')->paginate();
        
        return view('admin.contact.index', compact('data'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['book'] = $input['book'] == 'true' ? 1 : 0;
        $input['table'] = $input['table'] == 'true' ? 1 : 0;
              
        $message = Contact::create($input);
        $support = Support::find(1);

        if($support->email) {
            Mail::to($support->email)->send(new AdminContact($message));
        }
        Mail::to($input['email'])->send(new MailContact($message, $support));

        return response()->json(['data' => $message], 201);
    }
}
