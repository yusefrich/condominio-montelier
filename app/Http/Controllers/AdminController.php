<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Home1;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $home;

    public function __construct(Home1 $home) {
        $this->middleware('auth');
        $this->home = $home;
    }

    public function index()
    {
        $data = $this->home->orderBy('id', 'DESC')->paginate();

        return view('admin.home.index', compact('data'));
    }

    public function create()
    {
        return view('admin.home.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        //$input['url'] = $this->img($input, 'url', 'home', 0);
        if(isset($input['img'])) {
            $input['img'] = $this->img($input, 'img', 'home', 1);
        }

        $this->home->create($input);

        toastr()->success('Cadastrado com sucesso!');

        return redirect()->route('adm.home.index');
    }

    public function show($id)
    {
        $data = $this->home->find($id);

        return view('admin.home.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        if (isset($input['img'])) {
            $input['img'] = $this->img($input, 'img', 'home', 1);
        } else {
            unset($input['img']);
        }
        $this->home->find($id)->update($input);

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('adm.home.index');
    }

    public function destroy($id)
    {
        $this->home->find($id)->delete();

        toastr()->success('Apagado com sucesso!');

        return redirect()->route('adm.home.index');
    }

    public function panel()
    {
        $messages = Contact::orderBy('id', 'desc')->get(); 

        return view('admin.dashboard.index', compact('messages'));
    }
}
