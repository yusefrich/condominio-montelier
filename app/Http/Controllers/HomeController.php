<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use App\Models\Home1;
use App\Models\Home2;
use App\Models\Support;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $home;

    public function __construct(Home2 $home) {
        $this->middleware('auth')->except(['index', 'terms']);
        $this->home = $home;
    }

    public function admin()
    {
        $data = $this->home->paginate();

        return view('admin.home2.index', compact('data'));
    }

    public function create()
    {
        return view('admin.home2.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $input['img'] = $this->img($input, 'img', 'home');
        if (isset($input['background'])) {
            $input['background'] = $this->img($input, 'background', 'home');
        }
        $this->home->create($input);

        toastr()->success('Cadastrado com sucesso!');

        return redirect()->route('adm.home2.index');
    }

    public function show($id)
    {
        $data = $this->home->find($id);

        return view('admin.home2.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $input['img'] = $this->img($input, 'img', 'home');
        } else {
            unset($input['img']);
        }
        if (isset($input['background'])) {
            $input['background'] = $this->img($input, 'background', 'home');
        } else {
            unset($input['background']);
        }
        $this->home->find($id)->update($input);

        toastr()->success('Atualizado com sucesso!');

        return redirect()->route('adm.home2.index');
    }

    public function destroy($id)
    {
        $this->home->find($id)->delete();

        toastr()->success('Apagado com sucesso!');

        return redirect()->route('adm.home2.index');
    }

    public function index(Request $request)
    {
        $home1 = Home1::orderBy('id', 'DESC')->get()->groupBy('type');
        
        $home2 = $this->home->with('photos')->get()->toArray();
        
        $cons['years'] = Construction::all()->pluck('year', 'year')->toArray();
        $cons['months'] = Construction::all()->pluck('month_txt', 'month')->toArray();
        $first = Construction::latest()->first();
        $cons['default'] = $first ? $first->toArray() : ['year' => 1, 'month' => 0];
        $support = Support::find(1);
        
        return view('index', compact('home1', 'home2', 'cons', 'support'));
    }
    public function terms(Request $request)
    {
        $support = Support::find(1);

        return view('terms', compact('support'));
    }
}
