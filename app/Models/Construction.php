<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    protected $fillable = [
        'year', 'month', 'img', 'title'
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
      return url('storage/cons/'.$this->img);
    }

    public function getMonthTxtAttribute()
    {
        $arr = [
          '01' => 'JAN',
          '02' => 'FEV',
          '03' => 'MAR',
          '04' => 'ABR',
          '05' => 'MAI',
          '06' => 'JUN',
          '07' => 'JUL',
          '08' => 'AGO',
          '09' => 'SET',
          '10' => 'OUT',
          '11' => 'NOV',
          '12' => 'DEZ',
        ];

        return $arr[$this->month];
    }
}
