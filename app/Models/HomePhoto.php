<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomePhoto extends Model
{
    protected $table = 'home2_photos';

    protected $fillable = [
        'home_id', 'img', 'title'
    ];
}
